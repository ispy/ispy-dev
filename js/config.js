export const detector = {"Collections":{}};
export const version = "1.0.2-rc";

export const POINT = 0;
export const LINE = 1;
export const BOX = 2;
export const SOLIDBOX = 3;
export const SCALEDBOX = 4;
export const SCALEDSOLIDBOX = 5;
export const SCALEDSOLIDTOWER = 6;
export const MODEL = 7;
export const ASSOC = 8;
export const SHAPE = 9;
export const TEXT = 10;
export const BUFFERBOX = 11;
export const STACKEDTOWER = 12;
